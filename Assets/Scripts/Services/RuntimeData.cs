using System.Collections.Generic;
using Leopotam.Ecs;
using UnityEngine;

namespace Services
{
    public class RuntimeData
    {
        public Dictionary<EcsEntity, List<Transform>> PickUpAreasSpawnPositions =
            new Dictionary<EcsEntity, List<Transform>>();

        public Dictionary<EcsEntity, Stack<EcsEntity>> PickableItems = 
            new Dictionary<EcsEntity, Stack<EcsEntity>>();
    }
}