using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace MonoBehaviours
{
    public class PutDownAreaView : MonoBehaviour
    {
        [field: SerializeField] public Transform Transform {get; private set;}
        [field: SerializeField] public Transform ItemsPosition {get; private set;}
        
        public EcsWorld EcsWorld { get; set; }
        public EcsEntity PutDowAreaEntity { get; set; }

        private void OnTriggerEnter(Collider other)
        {
            ref var putDownAreaEnterEvent = ref EcsWorld.NewEntity().Get<PutDownAreaEnterEvent>();
            putDownAreaEnterEvent.PutDownAreaEntity = PutDowAreaEntity; 
            putDownAreaEnterEvent.EnterGameObject = other.gameObject;
        }

        private void OnTriggerExit(Collider other)
        {
            ref var putDownAreaExitEvent = ref EcsWorld.NewEntity().Get<PutDownAreaExitEvent>();
            putDownAreaExitEvent.PutDownAreaEntity = PutDowAreaEntity;
            putDownAreaExitEvent.ExitGameObject = other.gameObject;
        }
    }
}