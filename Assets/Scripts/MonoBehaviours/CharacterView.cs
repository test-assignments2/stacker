using DG.Tweening;
using Leopotam.Ecs;
using UnityEngine;

namespace MonoBehaviours
{
    public class CharacterView : MonoBehaviour
    {
        public EcsEntity Entity { get; set; }
        [field: SerializeField] public Transform Transform { get; private set; }
        [field: SerializeField] public CharacterController Controller { get; private set; }
        [field: SerializeField] public float Speed { get; private set; }
        [field: SerializeField] public Animator Animator { get; private set; }
        [field: SerializeField] public string MoveAnimationParameter { get; private set; }
        [field: SerializeField] public string CarryAnimationParameter { get; private set; }
        [field: SerializeField] public Transform StackPosition { get; private set; }
        [field: SerializeField] public int MaxItemQuantity { get; private set; }
        [field: SerializeField] public float MoveItemsAnimationDuration { get; private set; }
        [field: SerializeField] public Ease MoveItemsAnimationEase { get; private set; }
    }
}