using UnityEngine;

namespace MonoBehaviours
{
    public class CameraView : MonoBehaviour
    {
        [field: SerializeField] public Transform Transform {get; private set;}
        [field: SerializeField] public Vector3 FollowOffset {get; private set;}
        [field: SerializeField] public float SmoothTime {get; private set;}
    }
}