using System.Collections.Generic;
using UnityEngine;

namespace MonoBehaviours
{
    public class SceneData : MonoBehaviour
    {
        [field: SerializeField] public Joystick Joystick { get; private set; }
        [field: SerializeField] public List<PickUpAreaView> PickUpAreaViews { get; private set; }
        [field: SerializeField] public List<PutDownAreaView> PutDownAreaViews { get; private set; }
        [field: SerializeField] public UI.UI UI { get; private set; }
    }
}