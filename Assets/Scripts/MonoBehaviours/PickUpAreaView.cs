using System.Collections.Generic;
using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace MonoBehaviours
{
    public class PickUpAreaView : MonoBehaviour
    {
        [field: SerializeField] public Transform Transform { get; private set; }
        [field: SerializeField] public PickableItemView PickableItemPrefab { get; private set; }
        [field: SerializeField] public float SpawnItemDelay { get; private set; }
        [field: SerializeField] public List<Transform> ItemPositions { get; private set; }
        public EcsWorld EcsWorld { get; set; }
        public EcsEntity PickUpAreaEntity { get; set; }

        private void OnTriggerEnter(Collider other)
        {
            ref var pickUpAreaEnterEvent = ref EcsWorld.NewEntity().Get<PickUpAreaEnterEvent>();
            pickUpAreaEnterEvent.PickUpAreaEntity = PickUpAreaEntity; 
            pickUpAreaEnterEvent.EnterGameObject = other.gameObject;
        }

        private void OnTriggerExit(Collider other)
        {
            ref var pickUpAreaExitEvent = ref EcsWorld.NewEntity().Get<PickUpAreaExitEvent>();
            pickUpAreaExitEvent.PickUpAreaEntity = PickUpAreaEntity;
            pickUpAreaExitEvent.ExitGameObject = other.gameObject;
        }
    }
}