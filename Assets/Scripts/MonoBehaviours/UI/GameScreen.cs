using TMPro;
using UnityEngine;

namespace MonoBehaviours.UI
{
    public class GameScreen : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _onGround;
        [SerializeField] private TextMeshProUGUI _inHands;
        [SerializeField] private TextMeshProUGUI _inBuckets;

        public void SetOnGround(int quantity)
        {
            _onGround.text = quantity.ToString();
        }
        
        public void SetInHands(int quantity)
        {
            _inHands.text = quantity.ToString();
        }
        
        public void SetInBuckets(int quantity)
        {
            _inBuckets.text = quantity.ToString();
        }
    }
}