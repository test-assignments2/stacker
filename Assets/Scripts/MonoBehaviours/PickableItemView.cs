using UnityEngine;

namespace MonoBehaviours
{
    public class PickableItemView : MonoBehaviour
    {
        [field: SerializeField] public Transform NextSlotTransform { get; private set; }
    }
}