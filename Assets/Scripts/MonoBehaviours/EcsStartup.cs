using Leopotam.Ecs;
using Services;
using Systems;
using UnityEngine;

namespace MonoBehaviours
{
    public class EcsStartup : MonoBehaviour
    {
        [SerializeField] private PlayerView _playerView;
        [SerializeField] private SceneData _sceneData;
        
        private EcsWorld _ecsWorld;
        private EcsSystems _updateSystems;
        private RuntimeData _runtimeData;
        
        private void Start()
        {
            _ecsWorld = new EcsWorld();
            _updateSystems = new EcsSystems(_ecsWorld);
            _runtimeData = new RuntimeData();

            _updateSystems
                .Add(new PlayerInitSystem(_playerView))
                .Add(new PickUpAreaInitSystem(_sceneData.PickUpAreaViews))
                .Add(new PutDownAreaInitSystem(_sceneData.PutDownAreaViews))
                .Add(new ItemsCounterInitSystem())
                .Add(new PlayerInputSystem())
                .Add(new CharacterMoveSystem())
                .Add(new CharacterRotationSystem())
                .Add(new CharacterAnimationSystem())
                .Add(new FollowSystem())
                .Add(new ItemsGenerationSystem())
                .Add(new PickUpAreaEventsSystem())
                .Add(new PickingUpSystem())
                .Add(new PutDownAreaEventSystem())
                .Add(new PutingDownSystem())
                .Add(new UIUpdateSystem())
                .Inject(_sceneData)
                .Inject(_runtimeData);
            
            _updateSystems.Init();
        }

        private void Update()
        {
            _updateSystems?.Run();
        }

        private void OnDestroy()
        {
            _updateSystems?.Destroy();
            _updateSystems = null;
            _ecsWorld?.Destroy();
            _ecsWorld = null;
            _runtimeData = null;
        }
    }
}