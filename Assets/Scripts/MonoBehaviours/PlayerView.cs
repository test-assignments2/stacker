using UnityEngine;

namespace MonoBehaviours
{
    public class PlayerView : MonoBehaviour
    {
        [field: SerializeField] public CharacterView CharacterView { get; private set; }
        [field: SerializeField] public CameraView CameraView { get; private set; }
    }
}