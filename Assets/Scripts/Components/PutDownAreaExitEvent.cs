using Leopotam.Ecs;
using UnityEngine;

namespace Components
{
    public struct PutDownAreaExitEvent
    {
        public EcsEntity PutDownAreaEntity;
        public GameObject ExitGameObject;
    }
}