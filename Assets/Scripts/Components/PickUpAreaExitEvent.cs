using Leopotam.Ecs;
using UnityEngine;

namespace Components
{
    public struct PickUpAreaExitEvent
    {
        public EcsEntity PickUpAreaEntity;
        public GameObject ExitGameObject;
    }
}