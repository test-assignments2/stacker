using UnityEngine;

namespace Components
{
    public struct Follow
    {
        public Transform FollowerTransform;
        public Transform TargetTransform;
        public Vector3 FollowOffset;
        public float SmoothTime;
        public Vector3 CurrentVelocity;
    }
}