using UnityEngine;

namespace Components
{
    public struct PutDownArea
    {
        public Transform Transform;
        public Transform ItemsPosition;
        public int ItemsQuantity;
    }
}