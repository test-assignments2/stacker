namespace Components
{
    public struct ItemsCounter
    {
        public int OnGround;
        public int InHands;
        public int InBuckets;
    }
}