namespace Components
{
    public struct ItemGenerator
    {
        public float SpawnItemDelay;
        public float NextSpawnTime;
    }
}