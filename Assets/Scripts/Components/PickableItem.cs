using UnityEngine;

namespace Components
{
    public struct PickableItem
    {
        public Transform Transform;
        public Transform NextSlotTransform;
    }
}