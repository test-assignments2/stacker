using UnityEngine;

namespace Components
{
    public struct Move
    {
        public Vector3 Direction;
    }
}