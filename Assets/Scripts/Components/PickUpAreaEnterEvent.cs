using Leopotam.Ecs;
using UnityEngine;

namespace Components
{
    public struct PickUpAreaEnterEvent
    {
        public EcsEntity PickUpAreaEntity;
        public GameObject EnterGameObject;
    }
}