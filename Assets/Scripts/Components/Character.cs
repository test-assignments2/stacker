using DG.Tweening;
using UnityEngine;

namespace Components
{
    public struct Character
    {
        public Transform Transform;
        public CharacterController Controller;
        public float Speed;
        public Transform StackPosition;
        public int CurrentItemQuantity;
        public int MaxItemQuantity;
        public float MoveItemsAnimationDuration;
        public Ease MoveItemsAnimationEase;
    }
}