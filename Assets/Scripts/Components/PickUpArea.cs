using MonoBehaviours;
using UnityEngine;

namespace Components
{
    public struct PickUpArea
    {
        public Transform Transform;
        public int MaxItemsQuantity;
        public int ItemsQuantity;
        public PickableItemView PickableItemPrefab;
    }
}