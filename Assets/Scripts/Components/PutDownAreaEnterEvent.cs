using Leopotam.Ecs;
using UnityEngine;

namespace Components
{
    public struct PutDownAreaEnterEvent
    {
        public EcsEntity PutDownAreaEntity;
        public GameObject EnterGameObject;
    }
}