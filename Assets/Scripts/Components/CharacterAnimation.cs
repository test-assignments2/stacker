using UnityEngine;

namespace Components
{
    public struct CharacterAnimation
    {
        public Animator Animator;
        public int Move;
        public int Carry;
    }
}