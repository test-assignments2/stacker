using Components;
using DG.Tweening;
using Leopotam.Ecs;
using Services;

namespace Systems
{
    public class PutingDownSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Character, PutingDownItems> _putingDownItemsFilter;
        private readonly EcsFilter<ItemsCounter> _itemsCounterFilter;
        private readonly RuntimeData _runtimeData;
        
        public void Run()
        {
            foreach (var i in _putingDownItemsFilter)
            {
                ref var character = ref _putingDownItemsFilter.Get1(i);
                ref var putingDownItems = ref _putingDownItemsFilter.Get2(i);
                ref var putDownArea = ref putingDownItems.PutDownAreaEntity.Get<PutDownArea>();

                if (character.CurrentItemQuantity > 0)
                {
                    ref var characterEntity = ref _putingDownItemsFilter.GetEntity(i);

                    if (_runtimeData.PickableItems[putingDownItems.PutDownAreaEntity].Count > 0)
                    {
                        var lastPickableItem = _runtimeData.PickableItems[putingDownItems.PutDownAreaEntity].Peek().Get<PickableItem>();
                        if (DOTween.IsTweening(lastPickableItem.Transform))
                            continue;
                    }
                    
                    var pickableItemEntity = _runtimeData.PickableItems[characterEntity].Pop();
                    ref var pickableItem = ref pickableItemEntity.Get<PickableItem>();
                    
                    pickableItem.Transform.SetParent(putDownArea.Transform);

                    DOTween.Kill(pickableItem.Transform);
                    pickableItem.Transform.DOLocalJump(putDownArea.ItemsPosition.localPosition,
                            pickableItem.Transform.localPosition.y + 1f, 1, character.MoveItemsAnimationDuration)
                        .SetEase(character.MoveItemsAnimationEase);
                    
                    _runtimeData.PickableItems[putingDownItems.PutDownAreaEntity].Push(pickableItemEntity);

                    putDownArea.ItemsQuantity++;
                    character.CurrentItemQuantity--;
                    
                    foreach (var j in _itemsCounterFilter)
                    {
                        ref var itemCounter = ref _itemsCounterFilter.Get1(i);
                        itemCounter.InHands--;
                        itemCounter.InBuckets++;
                        _itemsCounterFilter.GetEntity(j).Get<UpdateUIEvent>();
                    }
                }
            }
        }
    }
}