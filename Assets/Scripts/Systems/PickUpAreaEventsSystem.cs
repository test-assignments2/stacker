using Components;
using Leopotam.Ecs;
using MonoBehaviours;

namespace Systems
{
    public class PickUpAreaEventsSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PickUpAreaEnterEvent> _enterEventFilter;
        private readonly EcsFilter<PickUpAreaExitEvent> _exitEventFilter;
        
        public void Run()
        {
            foreach (var i in _enterEventFilter)
            {
                ref var enterEvent = ref _enterEventFilter.Get1(i);
                if (enterEvent.EnterGameObject.TryGetComponent(out CharacterView characterView))
                {
                    ref var pickingUpItems = ref characterView.Entity.Get<PickingUpItems>();
                    pickingUpItems.PickUpAreaEntity = enterEvent.PickUpAreaEntity;
                }
                
                _enterEventFilter.GetEntity(i).Del<PickUpAreaEnterEvent>();
            }

            foreach (var i in _exitEventFilter)
            {
                ref var exitEvent = ref _exitEventFilter.Get1(i);
                if (exitEvent.ExitGameObject.TryGetComponent(out CharacterView characterView))
                {
                    characterView.Entity.Del<PickingUpItems>();
                }
                
                _exitEventFilter.GetEntity(i).Del<PickUpAreaExitEvent>();
            }
        }
    }
}