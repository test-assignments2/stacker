using Components;
using Leopotam.Ecs;
using MonoBehaviours;

namespace Systems
{
    public class UIUpdateSystem : IEcsRunSystem
    {
        private readonly EcsFilter<ItemsCounter, UpdateUIEvent> _uiUpdateFilter;
        private readonly SceneData _sceneData;
        
        public void Run()
        {
            foreach (var i in _uiUpdateFilter)
            {
                _uiUpdateFilter.GetEntity(i).Del<UpdateUIEvent>();
                
                ref var itemsCounter = ref _uiUpdateFilter.Get1(i);
                _sceneData.UI.GameScreen.SetOnGround(itemsCounter.OnGround);
                _sceneData.UI.GameScreen.SetInHands(itemsCounter.InHands);
                _sceneData.UI.GameScreen.SetInBuckets(itemsCounter.InBuckets);
            }
        }
    }
}