using Components;
using Leopotam.Ecs;

namespace Systems
{
    public class CharacterAnimationSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Character, CharacterAnimation, Move> _characterFilter;
        
        public void Run()
        {
            foreach (var i in _characterFilter)
            {
                ref var character = ref _characterFilter.Get1(i);
                ref var characterAnimation = ref _characterFilter.Get2(i);
                ref var move = ref _characterFilter.Get3(i);
                
                characterAnimation.Animator.SetFloat(characterAnimation.Move, move.Direction.magnitude);
                characterAnimation.Animator.SetBool(characterAnimation.Carry, character.CurrentItemQuantity > 0);
            }
        }
    }
}