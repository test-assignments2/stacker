using System.Collections.Generic;
using Components;
using Leopotam.Ecs;
using MonoBehaviours;
using Services;
using UnityEngine;

namespace Systems
{
    public class PlayerInitSystem : IEcsInitSystem
    {
        private readonly EcsWorld _ecsWorld;
        private readonly PlayerView _playerView;
        private readonly RuntimeData _runtimeData;
        
        public PlayerInitSystem(PlayerView playerView)
        {
            _playerView = playerView;
        }
        
        public void Init()
        {
            EcsEntity playerEntity = _ecsWorld.NewEntity();

            _playerView.CharacterView.Entity = playerEntity;

            ref var character = ref playerEntity.Get<Character>();
            character.Transform = _playerView.CharacterView.Transform;
            character.Controller = _playerView.CharacterView.Controller;
            character.Speed = _playerView.CharacterView.Speed;
            character.StackPosition = _playerView.CharacterView.StackPosition;
            character.MaxItemQuantity = _playerView.CharacterView.MaxItemQuantity;
            character.MoveItemsAnimationDuration = _playerView.CharacterView.MoveItemsAnimationDuration;
            character.MoveItemsAnimationEase = _playerView.CharacterView.MoveItemsAnimationEase;

            ref var animation = ref playerEntity.Get<CharacterAnimation>();
            animation.Animator = _playerView.CharacterView.Animator;
            animation.Move = Animator.StringToHash(_playerView.CharacterView.MoveAnimationParameter);
            animation.Carry = Animator.StringToHash(_playerView.CharacterView.CarryAnimationParameter);
            
            playerEntity.Get<Player>();
            playerEntity.Get<Move>();

            EcsEntity cameraEntity = _ecsWorld.NewEntity();

            ref var follow = ref cameraEntity.Get<Follow>();
            follow.FollowerTransform = _playerView.CameraView.Transform;
            follow.TargetTransform = _playerView.CharacterView.Transform;
            follow.FollowOffset = _playerView.CameraView.FollowOffset;
            follow.SmoothTime = _playerView.CameraView.SmoothTime;
            
            _runtimeData.PickableItems.Add(playerEntity,new Stack<EcsEntity>());
        }
    }
}