using Components;
using Leopotam.Ecs;

namespace Systems
{
    public class ItemsCounterInitSystem : IEcsInitSystem
    {
        private readonly EcsWorld _ecsWorld;
        
        public void Init()
        {
            var uiUpdateEntity = _ecsWorld.NewEntity();
            uiUpdateEntity.Get<ItemsCounter>();
        }
    }
}