using Components;
using Leopotam.Ecs;
using MonoBehaviours;
using UnityEngine;

namespace Systems
{
    public class PlayerInputSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Move, Player> _playerInputFilter;
        private readonly SceneData _sceneData;
        
        public void Run()
        {
            foreach (var i in _playerInputFilter)
            {
                ref var move = ref _playerInputFilter.Get1(i);
                move.Direction = new Vector3(_sceneData.Joystick.Horizontal, 0f, _sceneData.Joystick.Vertical);
            }
        }
    }
}