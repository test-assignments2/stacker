using System.Collections.Generic;
using Components;
using Leopotam.Ecs;
using MonoBehaviours;
using Services;
using UnityEngine;

namespace Systems
{
    public class PickUpAreaInitSystem : IEcsInitSystem
    {
        private readonly EcsWorld _ecsWorld;
        private readonly List<PickUpAreaView> _pickUpAreaViews;
        private readonly RuntimeData _runtimeData;
        public PickUpAreaInitSystem(List<PickUpAreaView> pickUpAreaViews)
        {
            _pickUpAreaViews = pickUpAreaViews;
        }
        
        public void Init()
        {
            foreach (var pickUpAreaView in _pickUpAreaViews)
            {
                var pickUpAreaEntity = _ecsWorld.NewEntity();
                pickUpAreaView.EcsWorld = _ecsWorld;
                pickUpAreaView.PickUpAreaEntity = pickUpAreaEntity;

                ref var pickUpArea = ref pickUpAreaEntity.Get<PickUpArea>();
                pickUpArea.Transform = pickUpAreaView.Transform;
                pickUpArea.MaxItemsQuantity = pickUpAreaView.ItemPositions.Count;
                pickUpArea.PickableItemPrefab = pickUpAreaView.PickableItemPrefab;
                _runtimeData.PickUpAreasSpawnPositions.Add(pickUpAreaEntity,pickUpAreaView.ItemPositions);

                ref var itemGenerator = ref pickUpAreaEntity.Get<ItemGenerator>();
                itemGenerator.SpawnItemDelay = pickUpAreaView.SpawnItemDelay;
                itemGenerator.NextSpawnTime = Time.time + itemGenerator.SpawnItemDelay;
                
                _runtimeData.PickableItems.Add(pickUpAreaEntity,new Stack<EcsEntity>());
            }
        }
    }
}