using System.Collections.Generic;
using Components;
using Leopotam.Ecs;
using MonoBehaviours;
using Services;

namespace Systems
{
    public class PutDownAreaInitSystem : IEcsInitSystem
    {
        private readonly EcsWorld _ecsWorld;
        private readonly List<PutDownAreaView> _putDownAreaViews;
        private readonly RuntimeData _runtimeData;

        public PutDownAreaInitSystem(List<PutDownAreaView> putDownAreaViews)
        {
            _putDownAreaViews = putDownAreaViews;
        }
        
        public void Init()
        {
            foreach (var putDownAreaView in _putDownAreaViews)
            {
                var putDownAreaEntity = _ecsWorld.NewEntity();
                putDownAreaView.EcsWorld = _ecsWorld;
                putDownAreaView.PutDowAreaEntity = putDownAreaEntity;

                ref var putDownArea = ref putDownAreaEntity.Get<PutDownArea>();
                putDownArea.Transform = putDownAreaView.Transform;
                putDownArea.ItemsPosition = putDownAreaView.ItemsPosition;
                
                _runtimeData.PickableItems.Add(putDownAreaEntity,new Stack<EcsEntity>());
            }
        }
    }
}