using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class CharacterMoveSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Character, Move> _playerFilter;

        public void Run()
        {
            foreach (var i in _playerFilter)
            {
                ref var character = ref _playerFilter.Get1(i);
                ref var move = ref _playerFilter.Get2(i);
                
                Vector3 motion = (Vector3.forward * move.Direction.z + Vector3.right * move.Direction.x) * character.Speed;
                character.Controller.Move((motion + Physics.gravity) * Time.deltaTime);
            }
        }
    }
}