using Components;
using Leopotam.Ecs;
using MonoBehaviours;

namespace Systems
{
    public class PutDownAreaEventSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PutDownAreaEnterEvent> _enterEventFilter;
        private readonly EcsFilter<PutDownAreaExitEvent> _exitEventFilter;
        
        public void Run()
        {
            foreach (var i in _enterEventFilter)
            {
                ref var enterEvent = ref _enterEventFilter.Get1(i);
                if (enterEvent.EnterGameObject.TryGetComponent(out CharacterView characterView))
                {
                    ref var putingDownItems = ref characterView.Entity.Get<PutingDownItems>();
                    putingDownItems.PutDownAreaEntity = enterEvent.PutDownAreaEntity;
                }
                
                _enterEventFilter.GetEntity(i).Del<PutDownAreaEnterEvent>();
            }

            foreach (var i in _exitEventFilter)
            {
                ref var exitEvent = ref _exitEventFilter.Get1(i);
                if (exitEvent.ExitGameObject.TryGetComponent(out CharacterView characterView))
                {
                    characterView.Entity.Del<PutingDownItems>();
                }
                
                _exitEventFilter.GetEntity(i).Del<PutDownAreaExitEvent>();
            }
        }
    }
}