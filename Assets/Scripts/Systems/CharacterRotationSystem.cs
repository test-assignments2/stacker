using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class CharacterRotationSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Character, Move> _playerFilter;
        
        public void Run()
        {
            foreach (var i in _playerFilter)
            {
                ref var character = ref _playerFilter.Get1(i);
                ref var move = ref _playerFilter.Get2(i);
                
                if (move.Direction != Vector3.zero)
                {
                    character.Transform.rotation = Quaternion.LookRotation(move.Direction);
                }
            }
        }
    }
}