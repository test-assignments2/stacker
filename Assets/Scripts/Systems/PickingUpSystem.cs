using Components;
using DG.Tweening;
using Leopotam.Ecs;
using Services;

namespace Systems
{
    public class PickingUpSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Character, PickingUpItems> _pickingUpItemsFilter;
        private readonly EcsFilter<ItemsCounter> _itemsCounterFilter;
        private readonly RuntimeData _runtimeData;
        
        public void Run()
        {
            foreach (var i in _pickingUpItemsFilter)
            {
                ref var character = ref _pickingUpItemsFilter.Get1(i);
                ref var pickingUpItems = ref _pickingUpItemsFilter.Get2(i);
                ref var pickUpArea = ref pickingUpItems.PickUpAreaEntity.Get<PickUpArea>();
    
                if (pickUpArea.ItemsQuantity > 0 && character.CurrentItemQuantity < character.MaxItemQuantity)
                {
                    ref var characterEntity = ref _pickingUpItemsFilter.GetEntity(i);

                    var targetParantTransform = character.Transform;
                    var targetLocalPosition = character.StackPosition.localPosition;
                    
                    if (character.CurrentItemQuantity > 0)
                    {
                        var lastPickableItem = _runtimeData.PickableItems[characterEntity].Peek().Get<PickableItem>();
                        
                        if (DOTween.IsTweening(lastPickableItem.Transform))
                            continue;

                        targetParantTransform = lastPickableItem.Transform;
                        targetLocalPosition = lastPickableItem.NextSlotTransform.localPosition;
                    }
                    
                    var pickableItemEntity = _runtimeData.PickableItems[pickingUpItems.PickUpAreaEntity].Pop();
                    ref var pickableItem = ref pickableItemEntity.Get<PickableItem>();

                    pickableItem.Transform.SetParent(targetParantTransform);
                    
                    DOTween.Kill(pickableItem.Transform);
                    pickableItem.Transform.DOLocalJump(targetLocalPosition, targetLocalPosition.y + 1f,
                        1,character.MoveItemsAnimationDuration)
                        .SetEase(character.MoveItemsAnimationEase);
                    
                    _runtimeData.PickableItems[characterEntity].Push(pickableItemEntity);

                    pickUpArea.ItemsQuantity--;
                    character.CurrentItemQuantity++;
                    
                    foreach (var j in _itemsCounterFilter)
                    {
                        ref var itemCounter = ref _itemsCounterFilter.Get1(i);
                        itemCounter.OnGround--;
                        itemCounter.InHands++;
                        _itemsCounterFilter.GetEntity(j).Get<UpdateUIEvent>();
                    }
                }
            }
        }
    }
}