using Components;
using Leopotam.Ecs;
using Services;
using UnityEngine;

namespace Systems
{
    public class ItemsGenerationSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PickUpArea, ItemGenerator> _pickupAreaFilter;
        private readonly EcsFilter<ItemsCounter> _itemsCounterFilter;
        private readonly EcsWorld _ecsWorld;
        private readonly RuntimeData _runtimeData;
        
        public void Run()
        {
            foreach (var i in _pickupAreaFilter)
            {
                ref var pickUpArea = ref _pickupAreaFilter.Get1(i);
                ref var itemGenerator = ref _pickupAreaFilter.Get2(i);

                if (IsGenerationNecessary(ref pickUpArea, ref itemGenerator))
                {
                    ref var pickUpAreaEntity = ref _pickupAreaFilter.GetEntity(i);
                    var position = _runtimeData.PickUpAreasSpawnPositions[pickUpAreaEntity][pickUpArea.ItemsQuantity].position;
                    var pickableItemView = Object.Instantiate(pickUpArea.PickableItemPrefab, position, Quaternion.identity);
                    pickableItemView.transform.SetParent(pickUpArea.Transform);
                    
                    var pickableItemEntity = _ecsWorld.NewEntity();
                    ref var pickableItem = ref pickableItemEntity.Get<PickableItem>();
                    pickableItem.Transform = pickableItemView.transform;
                    pickableItem.NextSlotTransform = pickableItemView.NextSlotTransform;
                    
                    _runtimeData.PickableItems[pickUpAreaEntity].Push(pickableItemEntity);
                    
                    itemGenerator.NextSpawnTime = Time.time + itemGenerator.SpawnItemDelay;
                    pickUpArea.ItemsQuantity++;
                    foreach (var j in _itemsCounterFilter)
                    {
                        ref var itemCounter = ref _itemsCounterFilter.Get1(i);
                        itemCounter.OnGround++;
                        _itemsCounterFilter.GetEntity(j).Get<UpdateUIEvent>();
                    }
                }
            }
        }

        private bool IsGenerationNecessary(ref PickUpArea pickUpArea, ref ItemGenerator itemGenerator)
        {
            return pickUpArea.ItemsQuantity < pickUpArea.MaxItemsQuantity && Time.time >= itemGenerator.NextSpawnTime;
        }
    }
}