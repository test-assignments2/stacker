using Components;
using Leopotam.Ecs;
using UnityEngine;

namespace Systems
{
    public class FollowSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Follow> _followFilter;
        
        public void Run()
        {
            foreach (var i in _followFilter)
            {
                ref var follow = ref _followFilter.Get1(i);
                
                var currentPos = Vector3.SmoothDamp(
                    follow.FollowerTransform.position,
                    follow.TargetTransform.position + follow.FollowOffset,
                    ref follow.CurrentVelocity, 
                    follow.SmoothTime);

                follow.FollowerTransform.position = currentPos;
            }
        }
    }
}